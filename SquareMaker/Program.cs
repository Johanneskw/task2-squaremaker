﻿using System;
using System.Data.Common;

namespace SquareMaker
{
    class Program
    {
        static void Main(string[] args)
        {   //Squaremaker using a 2d array. 
            Console.WriteLine("Hello, please input Height: ");
            int row = Int32.Parse(Console.ReadLine());

            Console.WriteLine("please input Width: ");
            int col = Int32.Parse(Console.ReadLine());

            Console.WriteLine();
            int[,] arr = new int[row, col];
            for (int i = 0; i<row; i++)
            {
                for (int j = 0; j<col; j++)
                {   //Checks if the index is borderindex.
                    if (i == 0 || i == row-1 || j == 0 || j == col-1)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
